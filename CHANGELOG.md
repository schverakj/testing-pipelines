# Changelog

<!--next-version-placeholder-->

## v0.2.0 (2022-04-07)
### Feature
* Test new pr pipeline ([`90123c4`](https://github.com/schverakj/testing-pipelines/commit/90123c48549c11e1836151b400159ac9b36fc495))
* Test new pr pipeline ([`c23df87`](https://github.com/schverakj/testing-pipelines/commit/c23df875870c938364efd2b132fb58cd176088d6))
* Test new pr pipeline ([`abf7b0c`](https://github.com/schverakj/testing-pipelines/commit/abf7b0c6309fb4327c898a4807ec604d143ed3c1))
* Test new pr pipeline ([`720755a`](https://github.com/schverakj/testing-pipelines/commit/720755ae99b1508b10809ffd5757672d9cd96a7c))
* Added pipeline to PRs ([`4835174`](https://github.com/schverakj/testing-pipelines/commit/48351741415c7b1cedbb24bf73eacc48360c7f90))

## v0.1.6 (2022-04-05)
### Fix
* Conventional commit ([`5182ffe`](https://github.com/schverakj/testing-pipelines/commit/5182ffebf3a911f2c923bd90f967691841ac6f77))

## v0.1.8 (2022-04-04)


## v0.1.7 (2022-04-04)


## v0.1.6 (2022-04-04)

